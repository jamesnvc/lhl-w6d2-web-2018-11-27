import React, { Component } from 'react';
import './App.css';
import Greeter from './Greeter.js';
import RepFinder from './RepFinder.js';
import Clock from './Clock.js';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {name: "James", whatever: "bloop"};
    this.doStuff = this.doStuff.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
  }

  doStuff() {
    alert(`hello ${this.state.name}!`);
  }

  handleNameChange(newName) {
    this.setState({name: newName});
  }

  // doStuff = () => {
  //   alert(`hello ${this.state.name}!`);
  // }

  render() {
    return (
      <div className="App">
        <button onClick={() => {
          /* this.setState({name: "??!?!"}); */
          this.setState((state, _props) => {
            state.name = state.name + "!";
            /* state.whatever = "saotnehusntoeah"; */
            return state;
            /* return {name: "hello", whatever: "bleep", someNewThing: 123}; */
          });
        }}>
          !!!
        </button>
        <span>{this.state.whatever}</span>
        <Greeter name={this.state.name} whatever={this.doStuff} onNameChange={this.handleNameChange} />
        <RepFinder />
        <Clock />
      </div>
    );
  }
}

export default App;
