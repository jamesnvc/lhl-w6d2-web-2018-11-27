import React, { Component } from 'react';

class RepFinder extends Component {
  constructor(props) {
    super(props);
    this.state = {reps: {representatives_centroid: []},
                  postalCode: "M5V1L5"};
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.runFetch(this.state.postalCode);
  }

  runFetch(postalCode) {
    fetch(`https://represent.opennorth.ca/postcodes/${postalCode}/`)
      .then((resp) => resp.json())
      .then((data) => {
        this.setState({reps: data});
        return true;
      })
      .catch((e) => {
        console.error("something went wrong", e);
      });
  }


  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.postalCode === prevState.postalCode) {
      return;
    }
    let postal = this.state.postalCode.toUpperCase().replace(/\s+/g, "");
    console.log("postal", postal);
    if (postal.length === 6) {
      this.runFetch(postal);
    }
  }

  handleChange(key) {
    return (e) => {
      this.setState({[key]: e.target.value});
    };
  }

  render() {
    return (
      <div>
        <input
          value={this.state.postalCode}
          onChange={this.handleChange('postalCode')}
        />
        <span>Postal Code: {this.state.postalCode}</span>
        <pre>{JSON.stringify(this.state.reps.representatives_centroid.map((r) => r.name))}</pre>
      </div>
    );
  }
}

export default RepFinder;
