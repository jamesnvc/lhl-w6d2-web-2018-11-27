import React, { Component } from 'react';

class Greeter extends Component {
  constructor(props) {
    super(props);
    this.state = {newName: props.name};
  }

  onNameChange = (e) => {
    let newName = e.target.value;
    // this.props.onNameChange(newName);
    this.setState({newName});
  }

  render() {
    return (
      <div>
        <span>Hello {this.props.name}</span>
        <label>Set Name
          <input
            value={this.state.newName}
            onChange={this.onNameChange}
          />
        </label>
        <button onClick={() => {
          this.props.onNameChange(this.state.newName)
        }}>Confirm Name Change</button>
        <button onClick={this.props.whatever}>
          Click Me
        </button>
      </div>
    );
  }
}

export default Greeter;
